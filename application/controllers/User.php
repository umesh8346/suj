<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';

class User extends REST_Controller {

    function __construct() {
       parent::__construct();
        $this->load->model('User_m');
            $this->load->helper(array('form','url','html','date'));
       
   

    }


        
        function UserRegistration_post(){
    
 $dateTime = new DateTime("now", new DateTimeZone('Asia/Kathmandu'));
   $date=$dateTime->format("Y m d");
            $parms=array();
            $parms['FirstName']=$this->post('FirstName');
            $parms['LastName']=$this->post('LastName');
           
               $parms['Email']=$this->post('Email');
            $parms['Address']=$this->post('Address');
            $parms['Phone']=$this->post('Phone');
                $parms['password']=$this->post('Password');
                $parms['createdDate']=$date;

      
            if ($this->User_m->insert($parms))
                {
                    $this->response(array('Status' => 'true','Data'=>$parms,'Message'=>'Successfully Registerd'),200);
                }
                else
                {
                    $this->response(array('Status' => 'true','Data'=>'','Message'=>"Please try again"));
                }


      }

       function UserLogin_Post(){

               $Email=$this->post('Email');
                $Password=$this->post('Password');
                $data=$this->User_m->getuserinfo($Email,$Password);
                if($data){
  $this->response(array('Status' => 'true','Data'=>$data,'Message'=>''),200);
                }else{
  $this->response(array('Status' => 'false','Data'=>'','Message'=>'Login Fail'),401);
                }
       }

 
}